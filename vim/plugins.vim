let data_dir = has('nvim') ? stdpath('data') .. '/site' : '~/.vim'
if empty(glob(data_dir .. '/autoload/plug.vim'))
  silent execute '!curl -fLo ' .. data_dir .. '/autoload/plug.vim --create-dirs '
    \ .. 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(data_dir .. '/plugged')
" Colorschemes
let themes_dir = data_dir .. '/pack/themes'
Plug 'veloce/vim-aldmeris', { 'on': [], 'dir': themes_dir .. '/opt/vim-aldmeris' }
Plug 'joshdick/onedark.vim', { 'dir': themes_dir .. '/start/onedark.vim' }
Plug 'tomasiser/vim-code-dark', { 'dir': themes_dir .. '/start/vim-code-dark' }

let lsp_dir = data_dir .. '/pack/lsp'
Plug 'neoclide/coc.nvim', { 'branch': 'release', 'dir': lsp_dir .. '/start/coc.nvim' }

let misc_dir = data_dir .. '/pack/misc'
Plug 'LunarWatcher/auto-pairs', { 'dir': misc_dir .. '/start/auto-pairs' }
Plug 'airblade/vim-gitgutter', { 'dir': misc_dir .. '/start/vim-gitgutter' }
Plug 'vim-airline/vim-airline', { 'dir': misc_dir .. '/start/vim-airline' }
	Plug 'ryanoasis/vim-devicons', { 'dir': misc_dir .. '/start/vim-devicons' }
	Plug 'majutsushi/tagbar', { 'dir': misc_dir .. '/start/tagbar' }
Plug 'yuttie/comfortable-motion.vim', { 'dir': misc_dir .. '/start/comfortable-motion.vim' }
Plug 'wellle/context.vim', { 'dir': misc_dir .. '/start/context.vim' }
call plug#end()
