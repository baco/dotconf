" Default extensions
let g:coc_global_extensions = [
  \ "coc-diagnostic",
  \ "@yaegassy/coc-ruff", "@yaegassy/coc-pylsp", "@yaegassy/coc-mypy", "coc-clangd", "coc-rust-analyzer"
  \ ]

" Keymaps
function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Use `[d` and `]d` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> ]d <Plug>(coc-diagnostic-next)
nmap <silent> [d <Plug>(coc-diagnostic-prev)

" GoTo code navigation
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> <Leader>D <Plug>(coc-type-definition)
nmap <silent> gri <Plug>(coc-implementation)
nmap <silent> grr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>

" Symbol renaming
nmap grn <Plug>(coc-rename)

" Formatting selected code
xmap <Leader>f  <Plug>(coc-format-selected)
nmap <Leader>f  <Plug>(coc-format-selected)

" Mappings for CoCList
" Document symbols
nnoremap <silent><nowait> gO  :<C-u>CocList outline<CR>
" Workspace symbols
nnoremap <silent><nowait> gW  :<C-u>CocList -I symbols<CR>
" Resume latest coc list
nnoremap <silent><nowait> <Space>p  :<C-u>CocListResume<CR>
