" Some nice defaults from Neovim

set tabpagemax=50

if empty($XDG_STATE_HOME)
	let $XDG_STATE_HOME = "~/.local/state"
endif
let g:stdpath_state = $XDG_STATE_HOME .. "/vim"
call mkdir(g:stdpath_state .. "/swap", "p", 0o700)
call mkdir(g:stdpath_state .. "/undo", "p", 0o700)
call mkdir(g:stdpath_state .. "/backup", "p", 0o700)

set directory^=$XDG_STATE_HOME/vim/swap//
set undodir^=$XDG_STATE_HOME/vim/undo
set backupdir^=$XDG_STATE_HOME/vim/backup
