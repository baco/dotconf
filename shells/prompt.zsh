get_distro() {
	local distro
	distro=$(. /etc/os-release && echo "$NAME")
	_RET="$distro"
}

prompt_colors() {
	get_distro
	local d="$_RET"
	case "$d" in
		Debian*)
			color0=$'%{\e[38;5;245m%}'
			color1=$'%{\e[38;5;125m%}'
			color2=$'%{\e[38;5;25m%}'
			color3=$'%{\e[38;5;197m%}'
			prompt_char="%(!.#.$)"
		;;
		Ubuntu*)
			color0=$'%{\e[38;5;245m%}'
			color1=$'%{\e[38;5;172m%}'
			color2=$'%{\e[38;5;5m%}'
			color3=$'%{\e[38;5;202m%}'
			prompt_char="%(!.#.⟫)"
		;;
		*)
			color0=""
			color1=""
			color2=""
			color3=""
			prompt_char="%#"
		;;
	esac
}

prompt off
prompt_colors
PS1="${color3}%(?..%? )%f${color0}%n%f@${color1}%m%f:${color2}%~%f${prompt_char}%b%f%k "

# vi: syntax=sh ts=4 noexpandtab
