Templates
=========

This directory bundles templates of config files for different kind of
projects.

The purpose of files here, is that for these tools there is no global/user
configuration that can be used as a common base, but instead, these tools can
only be configured per-project adding config files to the base dir of the
corresponding project.

Therefore, here are a bunch of files that with minimum or non changes can be
copied into project's base directories for configuring them.

There are templates for tools for different technologies.  Depending on
technologies used in each project is which files should be copied.
