BUILD ?= debug

CFLAGS ?= -Wall -Wextra -std=c23 -Wpedantic
cflags.debug = -g -DDEBUG
cflags.release = -O2 -Werror
CFLAGS += ${cflags.${BUILD}}
