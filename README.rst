Baco's Dotfiles
===============

These are my dot files.

Some Neovim/Vim specifics
-------------------------

`Neovim/Vim README <nvim/README.rst>`_


.. class:: center

~ Made with ❤️ and 🧉 ~


.. vim: se ft=rst
