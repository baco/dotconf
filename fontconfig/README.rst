Font customisations
===================

My preferred font for programming is Iosevka with the following look to match
Neovim GTK's settings:

.. image:: Iosevka_customized_glyphs.png

.. note::
   (this image is just for reference, in case Iosevka changes some of their
   character-variant codes to match the image.)
