(load-theme 'tango-dark t)
(setq display-line-numbers-type 'relative)
(save-place-mode 1)
(setq column-number-mode t)
(setq-default truncate-lines t)
(tool-bar-mode -1)

(set-face-attribute 'default nil
  :family "Iosevka Curly Slab"
  :slant 'normal
  :weight 'regular
  :height 113
  :width 'normal)

(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'eglot-ensure)  ; Requires LSP servers in PATH
(setq-default eglot-workspace-configuration
              '(:pylsp (:configurationSources ["flake8"]
                        :plugins (:flake8 (:enabled t)
                                  :pylint (:enabled t
                                           :executable "pylint")
                                  :pylsp_mypy (:overrides [t "--no-pretty"])
                                  :pycodestyle (:enabled :json-false)
                                  :pyflakes (:enabled :json-false)))))
(when window-system (set-frame-size (selected-frame) 85 60))

; After dealing with some nice defaults setup packages
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))

(use-package vscode-dark-plus-theme
  :ensure t
  :config
  (load-theme 'vscode-dark-plus t))

(use-package rust-mode :ensure t)
(use-package haskell-mode :ensure t)
