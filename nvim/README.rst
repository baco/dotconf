Tuning up Neovim/Vim
====================

Neovim - Vim compatibility
--------------------------

The idea is to keep both projects set up to behave, as close as possible, one
like the other.  Sometimes that requires translating from Lua to Vimscript, and
viceversa, in order to use the best possible solution for each editor.

Furthermore, when possible the same plugin is used for both editors.  But
sometimes it is necessary to use different solutions; that being either because
one of the editors is not capable of achieving the pretended behavior, or
because Neovim (being a more modern project) can achieve the expected behavior
in a better way.

Differences in plugins
''''''''''''''''''''''
====             ====                    ====
                 Neovim                  Vim
====             ====                    ====
*LSP client*     nvim-lspconfig          coc.nvim
\                \+ nvim-cmp             \
\                \+ mason-lspconfig.nvim \
\                \+ mason.nvim           \
*misc*           nvim-treesitter         -
*colorschemes*   vscode.nvim             vim-code-dark
\                onedark.nvim            onedark.vim
====             ====                    ====

Fonts with icon glyphs
----------------------

Some glyphs (icons) on Neovim/Vim might appear to be missing or broken!!! You
may need to use a patched font with the `NerdFonts <https://nerdfonts.com>`_
project.

Neovim/Vim config here has some automatic code completion.  Instead of using
words or letters for representing the different kind of completion items,
this config uses nice icons like popular IDEs to give some familiar look.

Easy fix
''''''''

Choose your favourite programming font (already patched) from `NerdFonts
<https://www.nerdfonts.com/font-downloads>`_, install it on your system and set
it as your terminal profile font.

Easier fix, yet (GNU/Linux only)
''''''''''''''''''''''''''''''''

Just download from NerdFonts the Symbols Only variant (NerdFontsSymbolsOnly)
and decompress it on your HOME in ``~/.local/share/fonts/``; then run
``fc-cache -frv`` to cache the new glyphs and restart your terminal (or app).

If you still have some mismatched symbols on your system, you can prioritize
the whole font *Symbols Nerd Font Mono* by symlinking
``fontconfig/fonts.conf`` into ``~/.config/fontconfig/`` provided in this repo.

(Alternatively)
~~~~~~~~~~~~~~~

You may prefer not to have symbols at all.  In that case, you have to:

1. In ``nvim/lsp_client_settings.vim`` file comment-out/delete:

   * the entry line ``formatting = { format = cmp_format }``

   * the 4 diagnostics lines starting with: ``sign define DiagnosticSign``

2. In ``vim/coc-settings.json`` delete the entry
   ``suggest.completionItemKindLabels``.

Not so easy fix: Patch your own font
''''''''''''''''''''''''''''''''''''

Install FontForge, for instance, on a Debian-based distro you can install this:
::

    $ sudo apt install python3-fontforge

Clone `NerdFonts <https://github.com/ryanoasis/nerd-fonts.git>`_ repository and
inside that directory run the following commands (This example assumes you are
patching Iosevka font, but you can patch one of your choice):
::

    ./nerd-fonts$ ls -u ~/.local/share/fonts/Iosevka/v15.6.2/ttf-unhinted/iosevka-curly-slab-{regular,bold,italic,bolditalic,extended{,bold,italic,bolditalic}}.ttf |
                      while read file
                      do
                          fontforge -script ./font-patcher -c -out=$HOME/.local/share/fonts/"Iosevka Nerd Font"/ -ext=otf $file
                      done

For other uses of the ``font-patcher`` tool you can read the `official
documentation
<https://github.com/ryanoasis/nerd-fonts#option-8-patch-your-own-font>`_.

Optional: (some requirements)
-----------------------------

For using Vim with coc.nvim plugin, or even some of the Neovim's native LSP
servers, you need to install Node+npm.  You can provide that on your system or,
if you don't have administrative permissions, follow `CoC's wiki instructions
<https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim#requirements>`_ for
requirements:
::

    $ curl -sL install-node.now.sh/lts | bash -s -- --prefix ~/.local


.. class:: center

~ Made with ❤️ and 🧉 ~


.. vim: se ft=rst
