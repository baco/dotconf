require'nvim-treesitter.configs'.setup {
  auto_install = true,
  highlight = { enable = true },
  incremental_selection = { enable = true },
  indent = { enable = true },

  -- textobjects treesitter plugin
  textobjects = {
    select = {
      enable = true,
      lookahead = true,
      keymaps = {
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ac"] = "@class.outer",
        ["ic"] = "@class.inner",
        ["as"] = { query = "@scope", query_group = "locals" },
      },
    },
  },
}

require'treesitter-context'.setup {
  max_lines = 2,
  min_window_height = 32,
  trim_scope = "inner",
}
