require("mason").setup()  -- Just for updating paths to mason packages on launch
require("mason-lspconfig").setup({ automatic_installation = true })

local lspconfig = require("lspconfig")
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local default_opts = { capabilities = capabilities }
local opts = require("lsp.server_settings")

lspconfig.pylsp.setup(vim.tbl_deep_extend("error", default_opts, opts.pylsp))
lspconfig.clangd.setup(default_opts)
lspconfig.rust_analyzer.setup(default_opts)
lspconfig.hls.setup(vim.tbl_deep_extend("error", default_opts, opts.hls))
lspconfig.fortls.setup(vim.tbl_deep_extend("error", default_opts, opts.fortls))

require("lsp.keymaps")
require("lsp.completion")
require("lsp.diagnostics")
if vim.lsp.inlay_hint then  -- Remove condition for v0.10+ release
  require("lsp.inlay")
end
