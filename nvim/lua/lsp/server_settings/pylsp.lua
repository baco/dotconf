--if vim.fn.exists(":PylspInstall") then
--  vim.cmd("PylspInstall pylsp-mypy")
--end
-- https://github.com/williamboman/mason.nvim/discussions/908
local pylsp = require("mason-registry").get_package("python-lsp-server")
pylsp:on("install:success", function ()
  local mason_package_path = function(package)
    local path = vim.fn.resolve(vim.fn.stdpath("data") .. "/mason/packages/" .. package)
    return path
  end

  local path = mason_package_path("python-lsp-server")
  local command = path .. "/venv/bin/python"

  local install_cmd = { command, "-m", "pip", "install" }
  local pylsp_plugins = { "python-lsp-ruff", "pylsp-mypy" }
  vim.schedule(function()
    vim.fn.system(vim.list_extend(install_cmd, pylsp_plugins))
  end)

  local flake8_plugins = { "flake8-pyproject", "flake8-pylint" }
  vim.schedule(function()
    vim.fn.system(vim.list_extend(install_cmd, flake8_plugins))
  end)
end)

local pylsp_settings = {
    configurationSources = { "flake8" },  -- tool's config files to override settings from
    plugins = {
        flake8 = { enabled = true },
        pylint = { enabled = true },
        pylsp_mypy = { overrides = { true, "--no-pretty" } },
        pycodestyle = { enabled = false },  -- covered in flake8
        pyflakes = { enabled = false },  -- covered in flake8
    },
}

local venv_dir = vim.env.VIRTUAL_ENV
if venv_dir ~= nil then
  local python_exec = vim.fn.findfile("python", venv_dir .. "/bin;")
  if python_exec ~= "" then
    pylsp_settings.plugins.jedi = {environment = python_exec}
  end
  local flake8_exec = vim.fn.findfile("flake8", venv_dir .. "/bin;")
  if flake8_exec ~= "" then
    pylsp_settings.plugins.flake8.executable = flake8_exec
  end
  local pylint_exec = vim.fn.findfile("pylint", venv_dir .. "/bin;")
  if pylint_exec ~= "" then
    pylsp_settings.plugins.pylint.executable = pylint_exec
  end
  local mypy_exec = vim.fn.findfile("mypy", venv_dir .. "/bin;")
  if mypy_exec ~= "" or python_exec ~= "" then
    pylsp_settings.plugins.pylsp_mypy.overrides = {"--python-executable", python_exec, true}
  end
end

return { settings = { pylsp = pylsp_settings } }
