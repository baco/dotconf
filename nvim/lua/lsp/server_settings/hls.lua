-- For plugins here to work properly refer to https://haskell-language-server.rtfd.io/en/latest/support/plugin-support.html#current-plugin-support-tiers
-- This documentation tells which GHC versions won't work for each plugin
local hls_settings = {
    formattingProvider = "fourmolu",
}
return { settings = { haskell = hls_settings } }
