local opts = {}
opts.pylsp = require("lsp.server_settings.pylsp")
opts.hls = require("lsp.server_settings.hls")
opts.fortls = require("lsp.server_settings.fortls")

local workspace_conf_file = vim.fn.findfile(".nvim/lsp-server-conf.lua", ".;")
if workspace_conf_file ~= "" then
    local workspace_conf = dofile(workspace_conf_file)
    opts = vim.tbl_deep_extend("force", opts, workspace_conf)
end

return opts
