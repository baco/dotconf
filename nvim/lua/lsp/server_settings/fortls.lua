local lspconfig = require("lspconfig")
local default_cmd = vim.deepcopy(lspconfig.fortls.document_config.default_config.cmd)

local cmd = vim.list_slice(default_cmd, nil, 1)
local default_flags = vim.list_slice(default_cmd, 3)  -- 1st flag dropped intentionally

local fortls_opts = {
  flags = vim.list_extend({ "--lowercase_intrinsics" }, default_flags)
}
return { cmd = vim.list_extend(cmd, fortls_opts.flags) }

-- In order to achive:
-- ===================
--local lsp = "fortls"
--settings[lsp] = { lowercaseIntrinsics = true }
--lspconfig[lsp].setup {
--  capabilities = capabilities,
--  settings = { ["fortran-ls"] = settings[lsp] },
--  root_dir = function(fname) return vim.fn.getcwd() end,
--  cmd = (function()
--    if lsp == "fortls" then
--      return vim.list_extend(
--        vim.deepcopy(lspconfig[lsp].document_config.default_config.cmd),
--        { "--lowercase_intrinsics" }
--      )
--    end
--  end)()}
