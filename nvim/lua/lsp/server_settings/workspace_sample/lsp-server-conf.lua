local pylsp_settings = {
  plugins = {
    flake8 = { maxLineLength = 120 },
    pylint = {
      args = {
        "--max-line-length=120",
        "--disable=missing-docstring,invalid-name,redefined-outer-name,logging-format-interpolation",
      },
    },
  },
}

local opts = {}
opts.pylsp = { settings = { pylsp = pylsp_settings } }

return opts
