vim.api.nvim_create_autocmd("LspAttach", {  -- Highly experimental textDocument/inlayHint not in PyLSP
  group = vim.api.nvim_create_augroup("UserLspConfig", { clear = false }),
  callback = function(args)
    if not (args.data and args.data.client_id) then
      return
    end

    vim.lsp.inlay_hint.enable(true)
  end,
})
