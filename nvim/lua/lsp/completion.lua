local cmp = require("cmp")
local cmp_format = require("lspkind").cmp_format {
  mode = "symbol",
  preset = "codicons",
  menu = {nvim_lsp = "[LSP]", buffer = "[Buffer]", path = "[Path]"}
}

vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", { clear = false }),
  callback = function(args)
    cmp.setup {
      mapping = {
        ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
        ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
        ["<CR>"] = cmp.mapping({
          i = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
          c = function(fallback)
            if cmp.visible() then
              cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true })
            else
              fallback()
            end
          end
        }),
        ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
      },
      formatting = {format = cmp_format},
      sources = {
        {name = "nvim_lsp"},
        {name = "buffer", keyword_length = 5},
        {name = "path"}
      },
      experimental = {ghost_text = true},
    }
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = "gitcommit",
  callback = function()
    cmp.setup.buffer {
      sources = {
        { name = "spell" },
        { name = "buffer", keyword_length = 5 },
        { name = "path" },
      },
    }
  end,
})
