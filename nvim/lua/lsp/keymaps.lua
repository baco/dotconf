vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", { clear = false }),
  callback = function(args)
    -- Mappings.
    local opts = { noremap=true, silent=true, buffer=args.buf }
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "<Leader>D", vim.lsp.buf.type_definition, opts)
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "<C-K>", vim.lsp.buf.signature_help, opts)

    vim.keymap.set("n", "gW", vim.lsp.buf.workspace_symbol, opts)
    vim.keymap.set("n", "<Leader>wa", vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set("n", "<Leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set("n", "<Leader>wl", function()
      vim.print(vim.lsp.buf.list_workspace_folders())
    end, opts)

    vim.keymap.set("n", "<Leader>q", vim.diagnostic.setloclist, opts)

    vim.keymap.set("n", "<Leader>f", function()
      vim.lsp.buf.format { async = true }
    end, opts)

    vim.keymap.set("n", "<Leader>ih", function()
      local filter = { bufnr = 0 }
      vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled(filter), filter)
    end, opts)

    if vim.fn.has("nvim-0.10") == 0 then  -- For backwards compatibility with v0.10+ defaults.
      vim.keymap.set("n", "grn", vim.lsp.buf.rename, opts)
      vim.keymap.set("n", "grr", vim.lsp.buf.references, opts)
      vim.keymap.set("n", "gri", vim.lsp.buf.implementation, opts)
      vim.keymap.set("n", "gO", vim.lsp.buf.document_symbol, opts)
      vim.keymap.set("i", "<C-S>", vim.lsp.buf.signature_help, opts)

      vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
      vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
      vim.keymap.set("n", "<C-W>d", vim.diagnostic.open_float, opts)
      vim.keymap.set("n", "<C-W><C-D>", vim.diagnostic.open_float, opts)
      vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
    end

    vim.keymap.set("n", "<Space>rn", function()
      vim.notify("Podrías usar `grn` (ver :h grn)\n", vim.log.levels.WARN)
      vim.lsp.buf.rename()
    end, opts)
    vim.keymap.set("n", "gr", function()
      vim.notify("Podrías usar `grr` (ver :h grr)", vim.log.levels.WARN)
      vim.lsp.buf.references()
    end, opts)
    vim.keymap.set("n", "gi", function()
      vim.notify("Podrías usar `gri` (ver :h gri)\n", vim.log.levels.WARN)
      vim.lsp.buf.implementation()
    end, opts)
    vim.keymap.set("n", "g0", function()
      vim.notify("Podrías usar `gO` (ver :h gO)", vim.log.levels.WARN)
      vim.lsp.buf.document_symbol()
    end, opts)
  end,
})
