if vim.fn.has("nvim-0.10") == 0 then  -- Remove block for v0.10+ release.  Covered by vim.diagnostic.config()
  vim.fn.sign_define("DiagnosticSignError", { text="", texthl="DiagnosticSignError", linehl="", numhl="" })
  vim.fn.sign_define("DiagnosticSignWarn", { text="", texthl="DiagnosticSignWarn", linehl="", numhl="" })
  vim.fn.sign_define("DiagnosticSignInfo", { text="", texthl="DiagnosticSignInfo", linehl="", numhl="" })
  vim.fn.sign_define("DiagnosticSignHint", { text="", texthl="DiagnosticSignHint", linehl="", numhl="" })
end
vim.diagnostic.config({
  signs = {
    text = {
      [vim.diagnostic.severity.ERROR] = "",
      [vim.diagnostic.severity.WARN] = "",
      [vim.diagnostic.severity.INFO] = "",
      [vim.diagnostic.severity.HINT] = "",
    },
  },

  update_in_insert = true,
  virtual_text = { source = "if_many", spacing = 24 },
  float = { source = "if_many", border = "rounded" },
})

vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", { clear = false }),
  callback = function()
    vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
      group = vim.api.nvim_create_augroup("Diagnostics", { clear = true }),
      callback = function()
        vim.diagnostic.open_float({
          focusable = false,
          header = "",
          scope = "cursor",
        })
      end,
    })
  end,
})
