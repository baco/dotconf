let data_dir = has('nvim') ? stdpath('data') .. '/site' : '~/.vim'
if empty(glob(data_dir .. '/autoload/plug.vim'))
  silent execute '!curl -fLo ' .. data_dir .. '/autoload/plug.vim --create-dirs '
    \ .. 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(data_dir .. '/plugged')
" Colorschemes
let themes_dir = data_dir .. '/pack/themes'
Plug 'veloce/vim-aldmeris', { 'on': [], 'dir': themes_dir .. '/opt/vim-aldmeris' }
Plug 'navarasu/onedark.nvim', { 'dir': themes_dir .. '/start/onedark.nvim' }
Plug 'Mofiqul/vscode.nvim', { 'dir': themes_dir .. '/start/vscode.nvim' }

let lsp_dir = data_dir .. '/pack/lsp'
Plug 'neovim/nvim-lspconfig', { 'dir': lsp_dir .. '/start/nvim-lspconfig' }
Plug 'hrsh7th/nvim-cmp', { 'on': ['InsertEnter'], 'dir': lsp_dir .. '/start/nvim-cmp' }
	Plug 'hrsh7th/cmp-nvim-lsp', { 'dir': lsp_dir .. '/start/cmp-nvim-lsp' }
	Plug 'hrsh7th/cmp-buffer', { 'dir': lsp_dir .. '/start/cmp-buffer' }
	Plug 'hrsh7th/cmp-path', { 'dir': lsp_dir .. '/start/cmp-path' }
	Plug 'onsails/lspkind-nvim', { 'dir': lsp_dir .. '/start/lspkind-nvim' }
Plug 'williamboman/mason-lspconfig.nvim', { 'dir': lsp_dir .. '/start/mason-lspconfig.nvim' }
	Plug 'williamboman/mason.nvim', { 'dir': lsp_dir .. '/start/mason.nvim' }

let misc_dir = data_dir .. '/pack/misc'
Plug 'LunarWatcher/auto-pairs', { 'dir': misc_dir .. '/start/auto-pairs' }
Plug 'lewis6991/gitsigns.nvim', { 'dir': misc_dir .. '/start/gitsigns.nvim' }
Plug 'vim-airline/vim-airline', { 'dir': misc_dir .. '/start/vim-airline' }
	Plug 'ryanoasis/vim-devicons', { 'dir': misc_dir .. '/start/vim-devicons' }
	Plug 'majutsushi/tagbar', { 'dir': misc_dir .. '/start/tagbar' }
	Plug 'tomasiser/vim-code-dark', { 'dir': themes_dir .. '/start/vim-code-dark' }
Plug 'yuttie/comfortable-motion.vim', { 'dir': misc_dir .. '/start/comfortable-motion.vim' }
Plug 'nvim-treesitter/nvim-treesitter', { 'dir': misc_dir .. '/start/nvim-treesitter' }
Plug 'nvim-treesitter/nvim-treesitter-textobjects', { 'dir': misc_dir .. '/start/nvim-treesitter-textobjects' }
Plug 'nvim-treesitter/nvim-treesitter-context', { 'dir': misc_dir .. '/start/nvim-treesitter-context' }
call plug#end()
