" Some settings Vim has on “defaults.vim” that Neovim hasn't set by default

set scrolloff=5

if has('mouse')
  set mouse=a
endif

augroup RestoreCursor
  autocmd!
  " This is slightly different than Vim's defaults.vim, but taken from
  " “:h restore-cursor” for Neovim
  autocmd BufRead * autocmd FileType <buffer> ++once
    \ let s:line = line("'\"")
    \ | if s:line >= 1 && s:line <= line("$") && &filetype !~# 'commit'
    \      && index(['xxd', 'gitrebase'], &filetype) == -1
    \ |   execute "normal! g`\""
    \ | endif
augroup END
