" Baco's Neovim init file

" Some defaults to start with
runtime! defaults.vim
if !has('vms') && has('persistent_undo')
	set undofile  " keep an undo file (undo changes after closing)
endif

" Essentials
set number
set relativenumber
set cursorline
set tabstop=4 shiftwidth=4
set softtabstop=-1  " Negative value sets to use "shiftwidth" instead
let mapleader = " "
let maplocalleader = "\\"

set list listchars=tab:→ ,trail:·,leadmultispace:⦙···
if has('autocmd')
	autocmd OptionSet shiftwidth execute
		\ 'setlocal listchars=tab:→ ,trail:·,'
		\ .. 'leadmultispace:⦙' .. repeat('·', &sw - 1)
endif
let &colorcolumn = join(range(80+1, 256), ',')

set clipboard+=unnamed,unnamedplus
vmap <LeftRelease> ygv

set switchbuf+=usetab,newtab

" Coding Styles
if has('autocmd')
	" Ensure spelling is loaded when writting text. Also start insert
	augroup GitStuff
		autocmd!
		autocmd FileType gitcommit setlocal spell nonumber norelativenumber
		autocmd FileType gitcommit startinsert
	augroup END
endif

" Extras
let g:vimsyn_embed = 'Pl'
let g:AutoPairsMapBS = v:true
let g:AutoPairsMultilineBackspace = v:true

runtime! plugins.vim
runtime! **/lsp/init.lua

if has('autocmd')
	augroup TerminalStuff
		autocmd!
		autocmd TermOpen * startinsert
		autocmd TermOpen * setlocal nonumber norelativenumber
		autocmd BufEnter * if &buftype == 'terminal' | :startinsert | endif
		autocmd TermClose * call feedkeys('\<cr>')
	augroup END
endif
command -nargs=? Terminal rightbelow 16split +terminal\ <args>
nnoremap <silent> <Leader>tt :rightbelow 16split +terminal<CR>
tnoremap <Esc> <C-\><C-N><C-W>p

set foldcolumn=auto:4
set foldlevelstart=99
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set foldtext=""
set nofoldenable

" Theming
let g:aldmeris_transparent = v:true
let g:aldmeris_termcolors = 'tango'

let g:onedark_config = {'transparent': v:true}
let g:onedark_terminal_italics = v:true

let g:vscode_style = 'dark'
let g:vscode_transparent = v:true
let g:vscode_italic_comment = v:true

" onedark.vim override: Don't set a background color when running in a
" terminal; just use the terminal's background color
if has('autocmd') && !has('gui_running')
	augroup colorset
		autocmd!
		autocmd ColorScheme onedark silent!
			\ call onedark#extend_highlight('Normal', {'bg': {'gui': 'NONE'}})
	augroup END
endif

silent! colorscheme vscode

if exists('g:colors_name') && (g:colors_name == 'onedark'
                             \ || g:colors_name == 'vscode')
	if has('termguicolors')
		set termguicolors
	endif
endif

" Misc
set laststatus=3
let g:airline#extensions#tabline#enabled = v:true
let g:airline#extensions#tabline#buffer_min_count = 2
let g:airline_mode_map = {
	\ '__': '-',
	\ 'n': 'N',
	\ 'i': 'I',
	\ 'R': 'R',
	\ 'c': 'C',
	\ 'v': 'V',
	\ 'V': 'V-L',
	\ '': 'V',
	\ 's': 'S',
	\ }
let g:airline#extensions#tagbar#flags = 'f'  " Requires majutsushi/tagbar plugin
let g:airline#extensions#nvimlsp#show_line_numbers = v:false
if exists('g:colors_name') && g:colors_name == 'vscode'
	let g:airline_theme = 'codedark'  " vscode.nvim doesn't provide one
endif
let g:DevIconsEnableDistro = v:false

lua require("treesitter")

runtime! gitsigns.vim
