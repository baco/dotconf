call rpcnotify(1, 'Gui', 'Font', 'Iosevka Curly Slab 11.5')
" Iosevka customizations: Haskell ligations; JetBrains Mono Style variant;
" cv-a-single-storey-serifed; cv-g-double-storey-open;
" cv-lig-ltgteq-slanted; cv-tilde-high; cv-underscore-low; cv-dollar-open;
" cv-percent-dots; cv-at-compact; cv-zero-slashed;
" cv-ampersand-et-toothless-corner; cv-brace-curly-flat-boundary;
call rpcnotify(1, 'Gui', 'FontFeatures', 'HSKL,ss14,cv26=8,cv32=2,VLAA=2,cv93=1,cv95=3,VSAC=1,VSAD=1,VSAB=9,cv81=2,VSAA=6,cv98=3')

if exists('g:GtkGuiLoaded')
	call rpcnotify(1, 'Gui', 'Command', 'PreferDarkTheme', 'on')
	call rpcnotify(1, 'Gui', 'Option', 'Popupmenu', 0)
endif

" Restore theme's background on GUI
let g:onedark_config = {'transparent': v:false}
if exists('g:colors_name') && g:colors_name == 'onedark'
	syntax reset
endif
if exists('g:colors_name') && g:colors_name == 'vscode'
	let g:vscode_transparent = v:false
lua <<EOF
	local c = require('vscode.colors').get_colors()
	require('vscode').setup({ group_overrides = { Normal = { fg=c.vscFront, bg=c.vscBack } } })
	require('vscode').load()
EOF
endif
