; extends

; Correct coloring group for class keyword:
; https://github.com/nvim-treesitter/nvim-treesitter/pull/3177#issuecomment-1186027468
"class" @keyword.function
