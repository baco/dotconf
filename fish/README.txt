First set the color theme, choosing (Base16 Eighties, Base16 Default Dark):

baco@terrapin ~> fish_config theme choose "Base16 Eighties"

After setting the desired theme, manually set some colors (values obtained
from https://jonasjacek.github.io/colors/):

baco@terrapin ~> set fish_color_user 8a8a8a
                 set fish_color_host af005f
                 set fish_color_cwd 005faf
                 set fish_color_status af00af
baco@terrapin ~>


Setting up the prompt layout requires patching the default function
fish_prompt.fish as specified in `prompt_layout_setup.txt`


Adding XDG_DATA_HOME/bin to the PATH:
baco@kodiak ~> fish_add_path ~/.local/bin
