if status is-interactive
    # Commands to run in interactive sessions can go here
    [ -r $HOME/.sh.d/aliases ] && . $HOME/.sh.d/aliases
end

set -gx EDITOR nvim
