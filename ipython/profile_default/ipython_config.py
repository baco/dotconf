# Load prompts
load_subconfig('ipython_prompts.py')

c.TerminalInteractiveShell.confirm_exit = False

c.TerminalIPythonApp.extensions = [
    'autoreload'
]
c.TerminalIPythonApp.exec_lines = ['%autoreload 2']
