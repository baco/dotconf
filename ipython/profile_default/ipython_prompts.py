"""usage:

from myprompt import MyPrompts
ip = get_ipython()
ip.prompts = MyPrompts(ip)
"""

from IPython.terminal.prompts import Prompts, Token


class MyPrompts(Prompts):

    def in_prompt_tokens(self, cli=None):
        return [
            (Token.Prompt, '['),
            (Token.PromptNum, str(self.shell.execution_count)),
            (Token.Prompt, ']>>> '),
        ]

    def out_prompt_tokens(self):
        return [
            (Token.OutPrompt, '['),
            (Token.OutPromptNum, str(self.shell.execution_count)),
            (Token.OutPrompt, ']<<< '),
        ]


c.TerminalInteractiveShell.prompts_class = MyPrompts
